class LogController < ActionController::Base

	LOG_FILE = "#{Rails.root}/public/logs.txt"
	def logs
		 # render :file LOG_FILE
		 render :inline => "<pre><%= File.read('#{LOG_FILE}') %></pre>"
	end

	def post_log
		content = "#{Time.now.strftime("%d/%m/%Y %H:%M:%S")}: #{params['log']}"
		File.open(LOG_FILE, "a+") do |f|
	  		f.puts(content)
		end

		render text: params["param"]

	end
end